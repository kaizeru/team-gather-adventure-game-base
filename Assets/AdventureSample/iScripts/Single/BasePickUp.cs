using UnityEngine;
using System.Collections;

public class BasePickUp : SingleIScript
{
	protected override void OnSuccess()
	{
		Helpers.Inventory.AddInteractable(Interactable);
	}
	
	protected override void OnOutOfRange()
	{
	}
}


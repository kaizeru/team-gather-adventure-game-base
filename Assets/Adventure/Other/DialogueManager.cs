using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogueManager : MonoBehaviour
{
	struct DialogueLineTime
	{
		public string line;
		public float time;
		public Renderer mesh;
		
		public DialogueLineTime(string line, float time, Renderer mesh)
		{
			this.line = line;
			
			if (time < 1f)
				this.time = 1f;
			else
				this.time = time;
			
			if (mesh == null)
				this.mesh = Player.Instance.PlayerMesh;
			else
				this.mesh = mesh;
		}
	}
	
	
	#region Constants
	
	
	static readonly int AVG_CHAR_WIDTH = 7;
	static readonly int LINE_HEIGHT = 25;
	
	
	#endregion
	
	
	#region Inspector variables
	
	
	public int DialogueYOffset;
	public float SecondsPerWord;
	
	
	#endregion
	
	
	#region Fields
	
	
	List<DialogueLineTime> dialogueToSpeak = new List<DialogueLineTime>();
	DialogueLineTime currentLine;
	float timeLineStarted;
	bool showingLine = false;
	
	
	#endregion
	
	
	#region Unity event handlers
	
	
	void Update()
	{
		// nothing to display
		if (dialogueToSpeak.Count <= 0)
			return;
		
		// hasn't finished current line
		if (Time.time < (timeLineStarted + currentLine.time))
			return;

		// finish current line
		dialogueToSpeak.Remove(currentLine);
		
		bool moreLinesLeft = (dialogueToSpeak.Count > 0);
			
		if (moreLinesLeft)
		{
			// move to next line
			StartShowingLine(dialogueToSpeak[0]);
		}
		else
		{
			showingLine = false;
		}
	}
	
	void OnGUI()
	{
		if (Event.current.type != EventType.Repaint)
			return;
		
		GUI.skin = Game.Instance.CustomSkin;
		
		if (dialogueToSpeak.Count <= 0)
			return;
		
		Vector3 middleTop = new Vector3(
			currentLine.mesh.bounds.center.x,
			currentLine.mesh.bounds.center.y + currentLine.mesh.bounds.extents.y,
			currentLine.mesh.bounds.center.z);
		Vector3 screenPos =
			Camera.mainCamera.WorldToScreenPoint(middleTop);
		
		int width = (currentLine.line.Length * AVG_CHAR_WIDTH);
		int height = LINE_HEIGHT;
		
		Rect dialogueBoxArea = new Rect(
			screenPos.x - (width / 2),
			Screen.height - screenPos.y - DialogueYOffset,
			width, height);
		
		GUI.Box(dialogueBoxArea, currentLine.line);
	}
	
	
	#endregion
	
	
	#region Public methods

	
	/// <summary>
	/// Makes object speak a single dialogue line,
	/// waiting for anyone else if they are talking.
	/// </summary>
	/// <param "speaker">
	/// Player if null, else specified object.
	/// </param>
	public void SpeakAfter(string line, Renderer speaker = null)
	{
		Speak(line, false, speaker);
	}
	
	/// <summary>
	/// Makes object speak a single dialogue line,
	/// interrupting anyone else if they are talking.
	/// </summary>
	/// <param "speaker">
	/// Player if null, else specified object.
	/// </param>
	public void SpeakNow(string line, Renderer speaker = null)
	{		
		Speak(line, true, speaker);
	}
	
	/// <summary>
	/// Makes object speak a set of dialogue line in sequence,
	/// waiting for anyone else if they are talking.
	/// </summary>
	/// <param "speaker">
	/// Player if null, else specified object.
	/// </param>
	public void SpeakAfter(string[] lines, Renderer speaker = null)
	{
		Speak(lines, false, speaker);
	}
	
	/// <summary>
	/// Makes object speak a set of dialogue line in sequence,
	/// interrupting anyone else if they are talking.
	/// </summary>
	/// <param "speaker">
	/// Player if null, else specified object.
	/// </param>
	public void SpeakNow(string[] lines, Renderer speaker = null)
	{		
		Speak(lines, true, speaker);
	}
	
	
	#endregion
	
	
	void Speak(string line, bool interrupt = true, Renderer speaker = null)
	{		
		int spaceNum = line.Split(' ').Length - 1;
		
		DialogueLineTime newLine = new DialogueLineTime(
			line, spaceNum * SecondsPerWord, speaker);
		
		if (interrupt)
		{
			dialogueToSpeak.Clear();
			showingLine = false;
		}
		
		dialogueToSpeak.Add(newLine);
		
		if (!showingLine)
			StartShowingLine(newLine);
	}
	
	void Speak(string[] lines, bool interrupt = true, Renderer speaker = null)
	{
		bool alreadyInterrupted = false;
		
		foreach (string l in lines)
		{
			Speak(l, interrupt, speaker);
			
			if (!alreadyInterrupted)
				alreadyInterrupted = true;
			
			if (alreadyInterrupted)
				interrupt = false;
		}
	}
	
	void StartShowingLine(DialogueLineTime lineTime)
	{
		showingLine = true;
		currentLine = lineTime;
		timeLineStarted = Time.time;
	}
}

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[AddComponentMenu("Adventure/Interactable")]
/// <summary>
/// A component that makes a game object interactable.
/// Behaviour scripts on all interactions with this interactable are individually
/// assignable and all behaviour script's parameters are visible/editable in this
/// interactable's inspector.
/// 
/// Cannot have a parent object if this object is ever to be in player's inventory (Unity's fault).
/// </summary>
/// <remarks>
/// Sealed (i.e., non-derivable) because any subclass will need another editor inspector 
/// script devoted specifically to that new subtype (Unity's fault).
/// </remarks>
public sealed class Interactable : MonoBehaviour
{
	#region Constants
	

	static readonly int CURSOR_PADDING = 15;
	static readonly int MENU_BORDER_PADDING = 5;
	static readonly int LINE_HEIGHT = 25;
	static readonly int AVG_CHAR_WIDTH = 9;
	
	
	#endregion
	
	
	#region Mode
	
	
	/// <summary>
	/// State of interaction with player.
	/// </summary>
	enum InteractableMode
	{
		/// <summary>
		/// Not being interacted with.
		/// </summary>
		Normal,
		/// <summary>
		/// Currently selected by player.
		/// </summary>
		Selected,
		/// <summary>
		/// Interactable's scene menu is open.
		/// </summary>
		SceneMenu,
	}
	
	InteractableMode mode = InteractableMode.Normal;
	
	
	#endregion
	
	
	#region Default inspector variables
	
	
	// shows in inspector
	[SerializeField]
	string description = Tags.Name;
	public string Description
	{
		get { return description; }
		set
		{
			// doesn't change
			if (value == description)
				return;
			
			description = value;
			UpdateSceneMenu();
		}
	}
	
	public float interactivityRange = 3f;
	public Texture icon;
	
	
	#endregion
	
	
	#region Other fields
	
	
	bool mouseIsInsideObject = false;
	bool inInventory = false;
	public bool InInventory
	{
		get { return inInventory; } 
	}
	bool destroyManuallyOnLoad = false;
	
	
	#endregion
	
	
	#region Other public methods
	
	
	/// <summary>
	/// Determines whether a position is within this object's interactive range.
	/// </summary>
	public bool IsInInteractiveRange(Vector3 position)
	{
		return Vector3.Distance(position, transform.position) <= interactivityRange;
	}
	
	/// <summary>
	/// Whether a position on screen is inside the object or menu (depends on which state this in),
	/// assuming (0, 0) is top-left of screen.
	/// </summary>
	/// <remarks>
	/// Useful for knowing whether player is clicking on the object or menu.
	/// </remarks>
	public bool IsScreenPointInside(Vector3 screenPoint)
	{
		if (mode == InteractableMode.Normal)
			return false;
		
		switch (mode)
		{
			case InteractableMode.Selected:
				return true;
			case InteractableMode.SceneMenu:
				if (sceneMenuBorderArea.Contains(screenPoint))
					return true;
				break;
		}
		
		return false;
	}
	
	/// <summary>
	/// Teleport this to a target object's position.
	/// </summary>
	public void Teleport(string targetName)
	{
		GameObject go = GameObject.Find(targetName);
		
		if (go == null)
		{
			Debug.LogError("No target object with name '" + targetName + 
				"' found, interactable teleport failed.");
		}
		else
		{
			Teleport(go);
		}
	}
	
	/// <summary>
	/// Teleport this to a target object's position.
	/// </summary>
	public void Teleport(GameObject go)
	{
		if (transform == null)
			return;
		
		if (InInventory)
		{
			Player.Instance.Inventory.RemoveInteractable(this, go);
		}
		else
		{
			transform.position = go.transform.position;
			transform.rotation = go.transform.rotation;
		}
	}
	
	
	/// <summary>
	/// Executes script assigned to interaction, returning whether able to occur.
	/// </summary>
	/// <remarks>Assumes interaction is actually on this interactable.</remarks>
	public bool ExecuteIScript(Interaction i, string verbName)
	{
		if (i == null)
			return false;
		
		if (i.Script != null)
		{
			i.Script.OnExecute(verbName);
			return true;
		}
		else
		{
			Debug.LogWarning("Nothing happened because no iScript attached " +
				"to verb '" + verbName + "' on '" + gameObject.name + "'");
			
			if (this.inInventory)
				Player.Instance.Inventory.TurnOffInteractableMenu();
			else
				SwitchFromSceneMenuMode();
			return false;
		}
	}
	
	/// <summary>
	/// Gets interaction that is currently set to a script.
	/// </summary>
	public Interaction GetInteraction(InteractionScript script)
	{
		return singleInteractions.Find(i => i.Script == script);
	}
	
	/// <summary>
	/// Gets single interaction that is set to a verb.
	/// </summary>
	public SingleInteraction GetInteraction(SingleInteraction.VerbType verb)
	{
		return singleInteractions.Find(i => i.Verb == verb);
	}
	
	/// <summary>
	/// Gets interaction that is set to a verb.
	/// </summary>
	public DualInteraction GetInteraction(DualInteraction.VerbType verb)
	{
		return dualInteractions.Find(i => i.Verb == verb);
	}
	
	
	#endregion
	
	
	#region Unity event handlers
	
	
	void Start()
	{
		description = Tags.ConvertTagsForObject(gameObject, description);
		UpdateSceneMenu();
	}
	
	void OnGUI()
	{
		// being managed by player's inventory
		if (InInventory)
			return;
		
		if (Game.Instance != null)
			GUI.skin = Game.Instance.CustomSkin;
		
		switch (mode)
		{
			case InteractableMode.Selected:
				DrawSelectionGUI();
				break;
			case InteractableMode.SceneMenu:
				DrawSceneMenuGUI();
				break;
		}
	}
	
	void OnLevelWasLoaded(int level)
	{
		if (destroyManuallyOnLoad)
			Destroy(gameObject);
	}
	
	void OnMouseEnter()
	{
		// being managed by player's inventory
		if (InInventory)
			return;
		
		mouseIsInsideObject = true;
		Player.Instance.SelectedInteractable = this;
		
		// player using another interactable's scene menu
		if (Player.Instance.InInteractableSceneMenu && (mode != InteractableMode.SceneMenu))
			return;
		
		// player using an interactable's inventory menu
		if (Player.Instance.Inventory.InInteractableMenu)
			return;
				
		// player in the middle of a dual interaction
		if (Player.Instance.InDualInteraction)
			return;
		
		if (mode == InteractableMode.Normal)
			SwitchToSelectedMode();
	}
	
	void OnMouseExit()
	{
		// being managed by player's inventory
		if (InInventory)
			return;
		
		mouseIsInsideObject = false;
		Player.Instance.SelectedInteractable = null;
		
		// player using another interactable's scene menu
		if (Player.Instance.InInteractableSceneMenu && (mode != InteractableMode.SceneMenu))
			return;
		
		// player using an interactable's inventory menu
		if (Player.Instance.Inventory.InInteractableMenu)
			return;
				
		// player in the middle of a dual interaction
		if (Player.Instance.InDualInteraction)
			return;
		
		if (mode == InteractableMode.Selected)
			SwitchToNormalMode();
	}
	
	
	void Update()
	{
		// being managed by player's inventory
		if (InInventory)
			return;		
		
		// only interested in main button press
		if (!Input.GetButtonDown(Game.Instance.MainButtonName))
			return;
		
		if (Player.Instance.InDualInteraction && 
			(Player.Instance.SelectedInteractable == this))
		{
			Player.Instance.ExecuteDualInteraction(this);
			return;
		}

		if (mode != InteractableMode.Selected)
			return;
		
		UpdateSceneMenu();
		
		if (sceneMenuInteractions.Count > 0)
			SwitchToSceneMenuMode();
	}
	
	#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.blue;
    	Gizmos.DrawWireSphere(transform.position, interactivityRange);
	}
	#endif
	
	
	#endregion
	
	
	#region Mode switching
	
	
	public void SwitchToNormalMode()
	{
		mode = InteractableMode.Normal;
	}
	
	public void SwitchToSelectedMode()
	{
		mode = InteractableMode.Selected;
	}
	
	public void SwitchToSceneMenuMode()
	{
		mode = InteractableMode.SceneMenu;
		
		Player.Instance.SwitchToInteractableSceneMenuMode();
	}
	
	public void SwitchFromSceneMenuMode()
	{
		if (mouseIsInsideObject)
			SwitchToSelectedMode();
		else
			SwitchToNormalMode();
		
		Player.Instance.SwitchFromInteractableSceneMenuMode();
	}
	
	
	#endregion
	
	
	#region Single interactions
	
	
	[SerializeField,HideInInspector]
	List<SingleInteraction> singleInteractions = new List<SingleInteraction>();
	// All possible single interactions
	public SingleInteraction[] SingleInteractions
	{
		get { return singleInteractions.ToArray(); }
	}
	
	[SerializeField,HideInInspector]
	SingleInteraction.VerbType singleVerbs;
	public SingleInteraction.VerbType SingleVerbs
	{
		get { return singleVerbs; }
		set
		{
			// didn't change
			if (value == singleVerbs)
				return;
			
			// see what interactions are (not) now selected
			foreach (SingleInteraction.VerbType t in 
				Enum.GetValues(typeof(SingleInteraction.VerbType)))
			{
				bool wasInTypes = (singleVerbs & t) == t;
				bool nowInTypes = (value & t) == t;
				
				SingleInteraction existing = singleInteractions.Find(i => (i.Verb == t));
				
				// remove from list
				if (wasInTypes && !nowInTypes)
				{
					if (existing != null)
						RemoveSingleInteraction(existing);
				}
				
				// add to list
				if (nowInTypes && !wasInTypes)
				{
					if (existing != null)
						RemoveSingleInteraction(existing);
					
					singleInteractions.Add(new SingleInteraction(gameObject, t));
				}
			}
			
			singleVerbs = value;
		}
	}

	void RemoveSingleInteraction(SingleInteraction i)
	{
		if (i == null)
			return;
		
		i.RemoveScript();
		
		singleInteractions.Remove(i);
	}
	
	#if UNITY_EDITOR
	[SerializeField,HideInInspector]
	public bool foldoutSingleSection = true;
	#endif
	
	
	#endregion
	
	
	#region Dual interactions
	
	
	[SerializeField,HideInInspector]
	List<DualInteraction> dualInteractions = new List<DualInteraction>();
	// All possible single interactions
	public DualInteraction[] DualInteractions
	{
		get { return dualInteractions.ToArray(); }
	}
	
	[SerializeField,HideInInspector]
	DualInteraction.VerbType dualVerbs;
	public DualInteraction.VerbType DualVerbs
	{
		get { return dualVerbs; }
		set
		{
			// didn't change
			if (value == dualVerbs)
				return;
			
			// see what interactions are (not) now selected
			foreach (DualInteraction.VerbType t in 
				Enum.GetValues(typeof(DualInteraction.VerbType)))
			{
				bool wasInTypes = (dualVerbs & t) == t;
				bool nowInTypes = (value & t) == t;
				
				DualInteraction existing = dualInteractions.Find(i => (i.Verb == t));
				
				// remove from list
				if (wasInTypes && !nowInTypes)
				{
					if (existing != null)
						RemoveDualInteraction(existing);
				}
				
				// add to list
				if (nowInTypes && !wasInTypes)
				{
					if (existing != null)
						RemoveDualInteraction(existing);
					
					dualInteractions.Add(new DualInteraction(gameObject, t));
				}
			}
			
			dualVerbs = value;
		}
	}
		
	void RemoveDualInteraction(DualInteraction i)
	{
		if (i == null)
			return;
		
		i.RemoveScript();
		
		dualInteractions.Remove(i);
	}
	
	#if UNITY_EDITOR
	[SerializeField,HideInInspector]
	public bool foldoutDualSection = true;
	#endif
	
	
	#endregion
	
	
	#region Scene menu
	
	
	Rect sceneMenuBorderArea;
	Rect sceneMenuButtonArea;
	List<Interaction> sceneMenuInteractions = new List<Interaction>();
	
	/// <summary>
	/// Updates both the contents and drawable area of scene menu.
	/// </summary>
	void UpdateSceneMenu()
	{
		sceneMenuInteractions = GetInteractionsAvailableInScene();
		UpdateSceneMenuArea();
	}
	
	/// <summary>
	/// Gets all possible interactions available when this object
	/// is in the scene.
	/// </summary>
	List<Interaction> GetInteractionsAvailableInScene()
	{
		List<Interaction> updatedSceneMenuInteractions = new List<Interaction>();
		
		foreach (Interaction i in singleInteractions)
			if (i.AvailableInScene)
				updatedSceneMenuInteractions.Add(i);
		
		foreach (Interaction i in dualInteractions)
			if (i.AvailableInScene)
				updatedSceneMenuInteractions.Add(i);
		
		return updatedSceneMenuInteractions;
	}
	
	/// <summary>
	/// Updates the drawable area of scene menu.
	/// </summary>
	void UpdateSceneMenuArea()
	{
		bool changedWidth = false;
		
		int buttonsWidth = description.Length * AVG_CHAR_WIDTH;
		int height = LINE_HEIGHT * (sceneMenuInteractions.Count + 2) + 
			(MENU_BORDER_PADDING * 2);
		
		sceneMenuBorderArea = new Rect(
			Input.mousePosition.x + CURSOR_PADDING,
			Screen.height - Input.mousePosition.y + CURSOR_PADDING,
			buttonsWidth + (MENU_BORDER_PADDING * 2),
			height);
		
		foreach (Interaction i in sceneMenuInteractions)
		{
			int iVerbLength = StringUtility.CamelToHuman(i.VerbAsString).Length * 
				AVG_CHAR_WIDTH;
			
			if (iVerbLength > buttonsWidth)
			{
				buttonsWidth = iVerbLength;
				changedWidth = true;
			}
		}
		
		sceneMenuButtonArea = new Rect(
			sceneMenuBorderArea.x + MENU_BORDER_PADDING,
			sceneMenuBorderArea.y + MENU_BORDER_PADDING + LINE_HEIGHT,
			buttonsWidth,
			height - LINE_HEIGHT - MENU_BORDER_PADDING);
		
		if (changedWidth)
		{
			sceneMenuBorderArea = new Rect(
				sceneMenuBorderArea.x,
				sceneMenuBorderArea.y,
				sceneMenuButtonArea.width + (MENU_BORDER_PADDING * 2),
				sceneMenuBorderArea.height);
		}
	}
	
	
	#endregion

	
	#region Inventory menu
	
	
	Rect inventoryMenuBorderArea;
	Rect inventoryMenuButtonArea;
	List<Interaction> inventoryMenuInteractions = new List<Interaction>();
	
	/// <summary>
	/// Updates both the contents and drawable area of inventory menu.
	/// </summay>
	/// <param name='blPoint'>
	/// Bottom-left most point on screen to position inventory menu.
	/// </param>
	public void UpdateInventoryMenu(Vector2 blPoint)
	{
		inventoryMenuInteractions = GetInteractionsAvailableInInventory();
		UpdateInventoryMenuArea(blPoint);
	}
	
	/// <summary>
	/// Gets all possible interactions available when this object
	/// is in the player's inventory.
	/// </summary>
	List<Interaction> GetInteractionsAvailableInInventory()
	{
		List<Interaction> updatedInventoryMenuInteractions = new List<Interaction>();
		
		foreach (Interaction i in singleInteractions)
			if (i.AvailableInInventory)
				updatedInventoryMenuInteractions.Add(i);
		
		foreach (Interaction i in dualInteractions)
			if (i.AvailableInInventory)
				updatedInventoryMenuInteractions.Add(i);
		
		return updatedInventoryMenuInteractions;
	}
	
	/// <summary>
	/// Updates the drawable area of inventory menu.
	/// </summary>
	void UpdateInventoryMenuArea(Vector2 blPoint)
	{
		bool changedWidth = false;
		
		int buttonsWidth = description.Length * AVG_CHAR_WIDTH + 20;
		int height = LINE_HEIGHT * (inventoryMenuInteractions.Count + 1) + 
			(MENU_BORDER_PADDING * 2);
		
		inventoryMenuBorderArea = new Rect(
			blPoint.x,
			blPoint.y - (MENU_BORDER_PADDING * 2) - height,
			buttonsWidth + (MENU_BORDER_PADDING * 2),
			height);
		
		foreach (Interaction i in inventoryMenuInteractions)
		{
			int verbLength = StringUtility.CamelToHuman(i.VerbAsString).Length * 
				AVG_CHAR_WIDTH;
			
			if (verbLength > buttonsWidth)
			{
				buttonsWidth = verbLength;
				changedWidth = true;
			}
		}
		
		inventoryMenuButtonArea = new Rect(
			inventoryMenuBorderArea.x + MENU_BORDER_PADDING,
			inventoryMenuBorderArea.y + MENU_BORDER_PADDING + LINE_HEIGHT,
			buttonsWidth,
			height - LINE_HEIGHT - MENU_BORDER_PADDING);
		
		if (changedWidth)
		{
			inventoryMenuBorderArea = new Rect(
				inventoryMenuBorderArea.x,
				inventoryMenuBorderArea.y,
				inventoryMenuButtonArea.width + (MENU_BORDER_PADDING * 2),
				inventoryMenuBorderArea.height);
		}
	}
	
	
	#endregion
	
	
	#region GUI helpers
	
	
	void DrawSelectionGUI()
	{
		if (Event.current.type != EventType.Repaint)
			return;
		
		string label = description;
		
		GUI.Box(new Rect(
			Input.mousePosition.x + CURSOR_PADDING, 
			Screen.height - Input.mousePosition.y + CURSOR_PADDING,
			sceneMenuBorderArea.width,
			LINE_HEIGHT),
			label);
	}
	
	void DrawSceneMenuGUI()
	{
		// background and title
		GUI.Box(sceneMenuBorderArea, description);
		
		GUILayout.BeginArea(sceneMenuButtonArea);
		GUILayout.BeginVertical();
	
		// scene interaction buttons
		foreach (Interaction i in sceneMenuInteractions)
		{
			string verbName = StringUtility.CamelToHuman(i.VerbAsString);

			if (GUILayout.Button(verbName))
			{
				// cancel current dual interaction
				if (Player.Instance.InDualInteraction)
					Player.Instance.SwitchFromDualInteractionMode();
				
				ExecuteIScript(i, verbName);
				
				if (i.Script is SingleIScript)
					SwitchFromSceneMenuMode();
			}
		}
		
		// cancel button
		if (GUILayout.Button("Cancel"))
			SwitchFromSceneMenuMode();
		
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}
	
	/// <summary>
	/// Draws the inventory menu GUI, using a screen position as
	/// the bottom-leftmost point, and returns whether a button was pressed.
	/// </summary>
	public bool DrawInventoryMenuGUI(Vector2 blPosition)
	{
		bool pressed = false;
		
		// background and title
		GUI.Box(inventoryMenuBorderArea, description);
		
		GUILayout.BeginArea(inventoryMenuButtonArea);
		GUILayout.BeginVertical();
	
		// inventory interaction buttons
		foreach (Interaction i in inventoryMenuInteractions)
		{
			string verbName = StringUtility.CamelToHuman(i.VerbAsString);
			
			if (GUILayout.Button(verbName))
			{
				ExecuteIScript(i, verbName);
				
				if (i.Script is SingleIScript)
					pressed = true;
			}
		}
		
		GUILayout.EndVertical();
		GUILayout.EndArea();
		
		return pressed;
	}
	
	
	#endregion
	
	
	#region Placing in scene / inventory
	
	
	/// <summary>
	/// Do what is required for this interactable to behave like 
	/// it's now in player's inventory (ie. not in the current scene).
	/// </summary>
	/// <remarks>
	/// Also changes parent object to player's inventory 
	/// so Unity will let it persist on scene change.
	/// </remarks>
	public void SetFromSceneToInventory()
	{
		inInventory = true;
		Helpers.SetGameObjectInactive(gameObject);
		
		transform.parent = Player.Instance.Inventory.transform;
		
		Game.Instance.RemoveObjectsWithNameFromScene(gameObject, true);
	}
	
	/// <summary>
	/// Do what is required for this interactable to behave like 
	/// it's now in the scene at a particular point
	/// (ie. not in player's inventory).
	/// </summary>
	public void SetFromInventoryToScene(Vector3 point)
	{		
		inInventory = false;
		Helpers.SetGameObjectActive(gameObject);
		
		transform.parent = null;
		transform.position = point;
		
		Game.Instance.AddObjectToScene(gameObject);
	}
	
	/// <summary>
	/// Do what is required for this interactable to behave like 
	/// it's now in the scene at an other object's position/rotation
	/// (ie. not in player's inventory).
	/// </summary>
	public void SetFromInventoryToScene(GameObject go)
	{
		SetFromInventoryToScene(go.transform.position);
		
		transform.rotation = go.transform.rotation;
	}
	
	
	#endregion
	
	
	#region Equality checking
	
	
	/// <summary>
	/// Compares equality of the single interactions.
	/// </summary>
	public bool CompareSingleInteractions(Interactable i)
	{
		if (i == null)
			return false;
		
		if (singleVerbs != i.singleVerbs)
			return false;
		
		if (singleInteractions.Count != i.singleInteractions.Count)
			return false;
		
		for (int x = 0; x < singleInteractions.Count; x++)
		{
			if (i.singleInteractions[x] == null)
				return false;
			
			if ((singleInteractions[x].AvailableInScene != 
					i.singleInteractions[x].AvailableInScene) ||
				(singleInteractions[x].AvailableInInventory !=
					i.singleInteractions[x].AvailableInInventory))
			{
				return false;
			}
		}
		
		return true;
	}
	
	/// <summary>
	/// Compares equality of the dual interactions.
	/// </summary>
	public bool CompareDualInteractions(Interactable i)
	{
		if (i == null)
			return false;
		
		if (dualVerbs != i.dualVerbs)
			return false;
		
		if (dualInteractions.Count != i.dualInteractions.Count)
			return false;
		
		for (int x = 0; x < dualInteractions.Count; x++)
		{
			if (i.dualInteractions[x] == null)
				return false;
			
			if ((dualInteractions[x].AvailableInScene != 
					i.dualInteractions[x].AvailableInScene) ||
				(dualInteractions[x].AvailableInInventory !=
					i.dualInteractions[x].AvailableInInventory))
			{
				return false;
			}
		}
		
		return true;
	}
	
	
	#endregion
}
